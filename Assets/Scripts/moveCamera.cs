using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCamera : MonoBehaviour
{
    public Transform target; // Transform del personaje a seguir
    public float followSpeed = 2f; // Velocidad de seguimiento
    public float yOffset = 1f; // Distancia vertical entre la c�mara y el personaje
    public float maxXOffset = 4f; // Distancia horizontal m�xima entre la c�mara y el personaje
    public float minXOffset = 2f; // Distancia horizontal m�nima entre la c�mara y el personaje

    private float xOffset; // Distancia horizontal actual entre la c�mara y el personaje

    void Start()
    {
        xOffset = maxXOffset;
    }

    void FixedUpdate()
    {
        float targetX = target.position.x;

        if (targetX > transform.position.x + xOffset)
        {
            // Mueve la c�mara hacia la derecha si el personaje se mueve a la derecha
            transform.position = new Vector3(targetX - xOffset, target.position.y + yOffset, transform.position.z);
        }
        else if (targetX < transform.position.x - xOffset)
        {
            // Mueve la c�mara hacia la izquierda si el personaje se mueve a la izquierda
            transform.position = new Vector3(targetX + xOffset, target.position.y + yOffset, transform.position.z);
        }
        else
        {
            // Mueve la c�mara suavemente si el personaje se mueve verticalmente
            transform.position = Vector3.Lerp(transform.position, new Vector3(targetX, target.position.y + yOffset, transform.position.z), Time.deltaTime * followSpeed);
        }

        // Limita la distancia horizontal entre la c�mara y el personaje
        xOffset = Mathf.Clamp(Mathf.Abs(transform.position.x - targetX), minXOffset, maxXOffset);
    }
}
