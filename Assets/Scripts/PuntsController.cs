using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class PuntsController : MonoBehaviour
{
    int punts;
    void Start()
    {
        punts = 0;
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "No tens punts...";
    }

    public void ActPunts()
    {
        punts += 100;
        this.GetComponent<TextMeshProUGUI>().text = "Punts: " + punts;
        if (punts % 10 == 0)
        {
            // Fer quelcom per augmentar la velocitat de les salses.
        }
    }
    public void ActVides(int vides)
    {
        //        this.gameObject.transform.GetChild(0).GetComponent<Transform>(). = new Vector3(vides, 1, 1);
        this.gameObject.transform.GetChild(vides).gameObject.SetActive(false);

    }


}