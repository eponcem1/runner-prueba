using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moviment : MonoBehaviour
{

    public Animator animator;
    public LayerMask groundLayer;
    private BoxCollider2D boxCollider;

    #region Running
    private float topSpeed = 6;
    private float acceleration = 2.5f;
    private float frictionSpeed = 2.5f;
    private float turnSpeed = 3f;
    float moveInput = 0;
    private Vector2 velocity;
    #endregion

    #region Jumping
    private float jumpPower = 4.5f;
    private float airAcceleration = 5;
    #endregion

    //Sprite Positioning
    [HideInInspector]
    public Transform t;
    bool facingRight = true;

    //Collision



    //raycaster set up
    Vector2 position;
    Vector2 position2;
    Vector2 position3;
    Vector2 direction = Vector2.down;
    float _distance = .19f;

    RaycastHit2D downRay1;
    RaycastHit2D downRay2;
    RaycastHit2D downRay3;



    bool IsGrounded()
    {
        if (downRay1.collider != null || downRay2.collider != null || downRay3.collider != null)
        {
            return true;
        }
        return false;
    }

    float CalcRot()
    {
        Vector2 average = ((downRay1.normal + downRay2.normal + downRay3.normal) / 3);
        float rotation = Mathf.Atan2(average.y, average.x) * Mathf.Rad2Deg;
        return rotation;
    }


    //states
    public bool walking;
    public bool jogging;
    public bool running;

    // Start is called before the first frame update
    void Start()
    {
        t = transform;
        facingRight = t.localScale.x > 0;
        boxCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        position = transform.position;
        position2 = new Vector2(position.x - 0.08f, position.y);
        position3 = new Vector2(position.x + 0.05f, position.y);
        Vector2 debugDistance = new Vector2(0f, -_distance);

        Debug.DrawRay(position, transform.TransformDirection(debugDistance), Color.red);
        Debug.DrawRay(position2, transform.TransformDirection(debugDistance), Color.red);
        Debug.DrawRay(position3, transform.TransformDirection(debugDistance), Color.red);

        downRay1 = Physics2D.Raycast(position, transform.TransformDirection(direction), _distance, groundLayer);
        downRay2 = Physics2D.Raycast(position2, transform.TransformDirection(direction), _distance, groundLayer);
        downRay3 = Physics2D.Raycast(position3, transform.TransformDirection(direction), _distance, groundLayer);

        if (IsGrounded())
        {
            Debug.Log(CalcRot());
            CalcRot();
        }


        //running
        moveInput = Input.GetAxisRaw("Horizontal");
        if (moveInput != 0)
        {
            if (IsGrounded())
            {
                velocity.x = Mathf.MoveTowards(velocity.x, topSpeed * moveInput, acceleration * Time.deltaTime);
            }
            else
            {
                velocity.x = Mathf.MoveTowards(velocity.x, topSpeed * moveInput, airAcceleration * Time.deltaTime);
            }

            //changing the way sprite is facing
            if (moveInput > 0 && !facingRight)
            {
                facingRight = true;
                t.localScale = new Vector3(Mathf.Abs(t.localScale.x), t.localScale.y, transform.localScale.z);
            }
            if (moveInput < 0 && facingRight)
            {
                facingRight = false;
                t.localScale = new Vector3(-Mathf.Abs(t.localScale.x), t.localScale.y, t.localScale.z);
            }
        }
        else
        {
            velocity.x = Mathf.MoveTowards(velocity.x, 0, frictionSpeed * Time.deltaTime);
        }

        if (moveInput < 0 && velocity.x > 0)
        {
            velocity.x = Mathf.MoveTowards(velocity.x, 0, turnSpeed * Time.deltaTime);
        }
        if (moveInput > 0 && velocity.x < 0)
        {
            velocity.x = Mathf.MoveTowards(velocity.x, 0, turnSpeed * Time.deltaTime);
        }

        animator.SetFloat("AnimSpeed", Mathf.Abs(velocity.x));




        //Jumping
        if (IsGrounded())
        {
            animator.SetBool("Grounded", true);
            velocity.y = 0;
            t.rotation = Quaternion.Euler(0, 0, CalcRot() - 90);

            if (Input.GetButtonDown("Jump"))
            {
                velocity.y = jumpPower;
                t.rotation = Quaternion.Euler(0, 0, 0);
                animator.SetBool("Grounded", false);
                animator.SetBool("IsJumping", true);
            }
        }
        else
        {
            //applying gravity
            velocity.y += Physics2D.gravity.y * Time.deltaTime;
            t.rotation = Quaternion.Euler(0, 0, 0);
        }

        if (!Input.GetButton("Jump") && !IsGrounded() && velocity.y > 2)
        {
            velocity.y = 2f;
        }

        if (velocity.y < 0 && !IsGrounded())
        {
            animator.SetBool("IsJumping", false);
            animator.SetBool("Grounded", false);
        }

        animator.SetFloat("AnimYSpeed", velocity.y);

        walking = Mathf.Abs(velocity.x) > 0 && Mathf.Abs(velocity.x) < 3;
        jogging = Mathf.Abs(velocity.x) > 3 && Mathf.Abs(velocity.x) < 6;
        running = Mathf.Abs(velocity.x) >= 6;

        t.Translate(velocity * Time.deltaTime);

        Collider2D[] hits = Physics2D.OverlapBoxAll(t.position, boxCollider.size, 0, groundLayer);

        foreach (Collider2D hit in hits)
        {
            if (hit == boxCollider)
                continue;

            ColliderDistance2D colliderDistance = hit.Distance(boxCollider);

            if (colliderDistance.isOverlapped)
            {
                t.Translate(colliderDistance.pointA - colliderDistance.pointB);
            }
        }

    
        if (IsGrounded())
        {
                Vector2 adjustment1 = new Vector2(0, _distance - downRay1.distance);
                Vector2 adjustment2 = new Vector2(0, _distance - downRay2.distance);
                Vector2 adjustment3 = new Vector2(0, _distance - downRay3.distance);
             
     
                if (downRay2.distance > downRay3.distance)
                {
                    t.Translate(0, adjustment2.y, 0);
             
                }
                    else if (downRay3.distance > downRay2.distance)
                    {
                         t.Translate(0, adjustment3.y, 0);
                    }
                        else if (downRay2 == downRay3)
                        {
                             t.Translate(0, adjustment1.y, 0);
                        }
             
        }
    }
}