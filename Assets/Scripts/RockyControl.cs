using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TextCore.Text;

public class RockyControl : MonoBehaviour
{
    public int vel;
    private bool tierra = false;
    private int salto = 0;
    private bool jump = false;
    private bool aire = false;
    public int vides;
    public PuntsController puntsManager;
    public GameObject PuntsController;
    // Update is called once per frame
    private void Start()
    {
        vides = 3;
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            this.jump = true;


            if (Input.GetKeyDown(KeyCode.W) && (this.tierra == true) | (this.salto < 1))
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 500f));
                this.gameObject.GetComponent<AudioSource>().Play();
                this.salto++;
            }
            this.gameObject.GetComponent<Animator>().Play("jump");

        }
        if (Input.GetKey(KeyCode.D))
        {
            if(this.vel < 15)
            {
                this.vel++;
            }
            

            if (this.aire == true && this.salto != 0)
            {
                this.gameObject.GetComponent<Animator>().Play("jump");
            }
            else if(this.tierra == true && this.salto == 0)
            { 
            this.gameObject.GetComponent<Animator>().Play("run");
            }
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            this.gameObject.GetComponent<AnimationClip>().GetComponent<SpriteRenderer>().flipX = false;
            
            
        }
        else if (Input.GetKey(KeyCode.A))
        {
            if (this.vel < 15)
            {
                this.vel++;
            }
            if (this.aire == true && this.salto !=0)
            {
                this.gameObject.GetComponent<Animator>().Play("jump");
            }
            else if (this.tierra == true && this.salto == 0)
            {
            this.gameObject.GetComponent<Animator>().Play("run");
            }
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            this.gameObject.GetComponent<AnimationClip>().GetComponent<SpriteRenderer>().flipX = true;
            
        }
        else if(this.tierra == true)
        {
            this.vel = 5;
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);

            if (Input.GetKey(KeyCode.S))
        {
            this.gameObject.GetComponent<Animator>().Play("agacharse");
           
        }else
            {
            this.gameObject.GetComponent<Animator>().Play("bored");
            }
            
        }

        
        


        

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Enemy" && this.jump == false)
        {
            vides--;
            puntsManager.ActVides(vides);
            StartCoroutine(perdreVida());
            if (vides <= 0)
            {
                print("gameover Mel�");
                SceneManager.LoadScene("GameOver");
                
            }
        }else if(collision.transform.tag =="Enemy" &&  this.jump == true)
        {
            Destroy(collision.gameObject);
            puntsManager.ActPunts();
            collision.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "muelle")
        {
            this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 1000f));
        }

    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            this.jump = false;
            this.aire = false;
            this.tierra = true;
            this.salto = 0;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            this.aire = true;
            this.tierra = false;
        }
    }

    IEnumerator perdreVida()
    {
        this.GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.2f);
        this.GetComponent<SpriteRenderer>().color = Color.white;
        yield return new WaitForSeconds(0.2f);
        this.GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.2f);
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }
}
