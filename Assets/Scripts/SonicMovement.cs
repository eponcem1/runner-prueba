using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonicMovement : MonoBehaviour
{
    [SerializeField] private float slopeAngleLimit = 45f; // �ngulo m�ximo de inclinaci�n de la pendiente
    [SerializeField] private float slopeGravityMultiplier = 1.5f; // Multiplicador de gravedad en pendientes
    [SerializeField] private float minGroundNormalY = 0.65f; // Valor m�nimo de normal Y para considerar el suelo
    private Rigidbody2D rb;
    private Vector2 groundNormal;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        // Verificar si el personaje est� en el suelo
        bool isGrounded = false;
        Vector2 groundCheckOrigin = (Vector2)transform.position + Vector2.down * 0.5f;
        RaycastHit2D[] hits = Physics2D.RaycastAll(groundCheckOrigin, Vector2.down, 0.1f);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.gameObject != gameObject && hits[i].normal.y >= minGroundNormalY)
            {
                isGrounded = true;
                groundNormal = hits[i].normal;
            }
        }

        // Aplicar gravedad normal o en pendientes
        if (isGrounded)
        {
            // Aplicar gravedad normal
            Vector2 gravity = new Vector2(0f, Physics2D.gravity.y * rb.gravityScale);
            rb.AddForce(gravity);
        }
        else
        {
            // Aplicar gravedad en pendientes
            Vector2 slopeGravity = new Vector2(0f, Physics2D.gravity.y * slopeGravityMultiplier * rb.gravityScale);
            rb.AddForce(slopeGravity);
        }

        // Aplicar correcci�n de pendientes
        if (isGrounded && groundNormal.x != 0f)
        {
            float slopeAngle = Vector2.Angle(groundNormal, Vector2.up);
            if (slopeAngle > slopeAngleLimit)
            {
                float slopeDirection = Mathf.Sign(groundNormal.x);
                Vector2 slopeForce = new Vector2(slopeDirection * slopeGravityMultiplier * rb.mass, -slopeDirection * rb.mass);
                rb.AddForce(slopeForce);
            }
        }
    }
}
